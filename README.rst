==============
Gladstone Demo
==============
A demo game for Gladstone
-------------------------

Gladstone is a framework for building turn-based tactics games in the LÖVE_ framework.  The framework and the demo don't do much yet.

.. _LÖVE: https://love2d.org/
