-- This file is part of Gladstone Demo, the demo game for Gladstone
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Declare library variables at the top (file) level so that they are visible to
-- all the LÖVE callback functions, and actually "require" the libraries in
-- love.load().

local gladstone

DEBUG = true

--- Game state ---
local board

--- GUI state ---
local gui_state = {
  message = nil,
  pointUnderMouse = nil,
  board = {
    gridSquareSize = 45,
    gridSquarePadding = 10,
    -- Tables of points to be highlighted in the GUI for debugging purposes. All
    -- are lists of points.
    markedPoints = {
      -- Points touched by the last line-of-sight operation.
      lineOfSight = {},
      -- Points reachable by the last checked unit.
      reachable = {}
    },
    selected = nil
  }
}

--- LÖVE callbacks ---
function love.load()
  love.filesystem.setRequirePath(love.filesystem.getRequirePath() .. ";lib/?.lua;lib/?/init.lua")
  gladstone = require "gladstone"

  -- Set up debugging callbacks
  function gladstone.board.debug_callbacks.lineOfSight(points)
    gui_state.board.markedPoints.lineOfSight = points
  end

  love.graphics.setFont(love.graphics.newFont(18))

  board = gladstone.board.makeBoard(20, 15)

  board.units[{ 5, 3}] = {
    heading = 0,
    moved = false
  }
  board.units[{10, 6}] = {
    heading = -90,
  }
  gladstone.ai.makeWatcher({x = 10, y = 6}, board)

  board.barriers[{ 6, 4}] = true
  board.barriers[{10, 7}] = true

  -- Initial graphics setup.
  love.graphics.setBackgroundColor(0xFF, 0xFF, 0xFF) -- White
  love.window.setMode(1024, 768)
end

function love.update(_)
  gladstone.gui.updateSUIT(gui_state, board)
end

function love.draw()
  gladstone.gui.draw(gui_state, board)
end

function love.mousepressed(x, y, button)
  gladstone.gui.onMousePress(gui_state, board, x, y, button)
end

function love.mousemoved(x, y, dx, dy)
  gladstone.gui.onMouseMove(gui_state, board, x, y, dx, dy)
end
